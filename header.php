<!doctype html>

  <html id="html" class="no-js" <?php language_attributes(); ?>>

	<head>
		<meta charset="utf-8">

		<!-- Force IE to use the latest rendering engine available -->
		<meta http-equiv="X-UA-Compatible" content="IE=edge">

		<!-- Mobile Meta -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta class="foundation-mq">

		<!-- If Site Icon isn't set in customizer -->
		<?php if ( ! function_exists( 'has_site_icon' ) || ! has_site_icon() ) { ?>
			<!-- Icons & Favicons -->
			<link rel="icon" href="<?php echo get_template_directory_uri(); ?>/favicon.png?v=3">
			<link href="<?php echo get_template_directory_uri(); ?>/assets/images/apple-icon-touch.png?v=3" rel="apple-touch-icon" />
			<!--[if IE]>
				<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico?v=3">
			<![endif]-->
			<meta name="msapplication-TileColor" content="#f01d4f">
			<meta name="msapplication-TileImage" content="<?php echo get_template_directory_uri(); ?>/assets/images/win8-tile-icon.png?v=3">
	    	<meta name="theme-color" content="#121212">
	    <?php } ?>

		<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">

		<?php wp_head(); ?>

		<!-- Drop Google Analytics here -->
		<!-- end analytics -->

	</head>

	<!-- Uncomment this line if using the Off-Canvas Menu -->

	<body <?php body_class(); ?>>

		<div class="off-canvas-wrapper">

			<div class="off-canvas-wrapper-inner" data-off-canvas-wrapper>

				<?php get_template_part( 'parts/content', 'offcanvas-right' ); ?>
				<?php get_template_part( 'parts/content', 'offcanvas-left' ); ?>

				<div class="off-canvas-content" data-off-canvas-content>

					<header class="header" role="banner">

						<div class="callout large primary">
							<div class="row column text-center">
								<h2><?php bloginfo('name'); ?></h2>
								<h3 class="subheader"><?php bloginfo('description'); ?></h3>
							</div>
						</div>

						<?php //get_template_part( 'parts/nav', 'offcanvas-topbar' ); ?>
						<?php //get_template_part( 'parts/nav', 'topbar' ); ?>
						<?php //get_template_part( 'parts/nav', 'alternate' ); ?>
						<?php get_template_part( 'parts/nav', 'sticky-topbar' ); ?>

					</header> <!-- end .header -->