<div id="stickyTopAnchor" data-sticky-container>
	<div class="top-bar sticky" data-sticky data-margin-top="0" data-margin-bottom="0" data-top-anchor="stickyTopAnchor" data-btm-anchor="stickyBtmAnchor" data-options="stickyOn:small;">
		<div class="row">
			<nav class="top-bar-left">
				<ul class="dropdown menu" data-dropdown-menu>
					<li class="home-link float-left">
						<a href="<?php echo home_url(); ?>"><?php bloginfo('name'); ?></a>
					</li>
					<li class="float-right" data-responsive-toggle="main-menu" data-hide-for="medium">
						<a href="#" data-toggle>Menu</a>
					</li>
				</ul>
			</nav>
			<nav class="top-bar-right" id="main-menu">
				<?php rufio_t_nav(); ?>
			</nav>
		</div>
	</div>
</div>